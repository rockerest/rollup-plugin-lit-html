# rollup-plugin-lit-html

Use plain HTML files as lit-html templates.  
Keep your markup and your logic separate!

##Installation  

`npm install rollup-plugin-lit-html`

## Usage

- Add `rollup-plugin-lit-html` to your rollup configuration:

```
// rollup.config.js
import litHtml from 'rollup-plugin-lit-html';

export default {
  input: 'src/main.js',
  output: {
    file: 'bundle.js',
    format: 'esm'
  },
  plugins: [ litHtml() ]
};
```

Now you can write "plain" html and use `lit-html` to easily manage your DOM without mixing JavaScript and HTML.

> *_NOTE_*:  
> `lit-html` is extremely sensitive to multiple copies running in the same application.
>
> You *must* provide your own copy of `lit-html` to avoid this problem.

## "API"

Instead of importing a string of HTML, you will import a template function.

```
import { template } from "./main.html";
```

The `template` function's signature is  
```
template( configuration )
```

`configuration` is a required object that must contain at least these two values:  
| Property | Type | Default |
| ---------|------|-------- |
| `values` | Object | `{}` |
| `html` | Function | `( str ) => str.raw` |

`values` should be a list of values to expand into the HTML.

So if your `lit-html` use-case _was_:
```
// src/main.js
import { html, render } from "lit-html";

var descriptor = "neat";

render( html`<span>This is ${descriptor}</span>`, document.body );

// Boo, mixed JS and HTML!
```

Now it can be:

```
// src/main.html
<span>This is ${descriptor}</span>
```
```
// src/main.js
import{ render, html } from "lit-html";
import { template } from "./main.html";

var descriptor = "neat";

render( template( { "values": { descriptor }, html } ), document.body );

// Hooray, separation!
```

----

`rollup-plugin-lit-html` will play nice with other html/string loaders, just exclude or include what you need:

```
// rollup.config.js
import litHtml from 'rollup-plugin-lit-html';

export default {
  input: 'src/main.js',
  output: {
    file: 'bundle.js',
    format: 'esm'
  },
  plugins: [
    litHtml( {
      "include": "**/*.template.html",
      "exclude": "static/**/*.html"
    } )
  ]
};
```