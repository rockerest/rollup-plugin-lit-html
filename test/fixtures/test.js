import { TemplateResult, html } from "../../node_modules/lit-html/lit-html.js";

import { template } from "./template.html";

var fill = "test";
var lit = template( { "values": { fill }, html } );

assert.equal( typeof template, "function" );
assert.ok( lit instanceof TemplateResult );
assert.equal( lit.strings[ 0 ], "text content filled with " );
assert.equal( lit.strings.length, 2 );
assert.equal( lit.values[ 0 ], fill );
assert.equal( lit.type, "html" );