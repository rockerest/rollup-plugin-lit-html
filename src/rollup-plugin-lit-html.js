import { createFilter } from "rollup-pluginutils";

function template( code ){
	var clean = code
		.replace( /\\/g, '\\' )
		.replace( /\$\{/g, '\\${' )
		.replace( /([^\\])`/g, '$1\\`' );

	return `export var template = ( { values = {}, html = ( str ) => str.raw } = {} ) => {
	var list = values ? Object.keys( values ).join( ", " ) : "";
	var extractorBody = \`
		"use strict";

		if( "$\{list}" ){
			var { $\{list} } = params;
		}

		return html\\\`${clean}\\\`;
	\`;

	return ( new Function( "params", "html", extractorBody ) )( values, html );
}`;
}

export default function litHtml( options = {} ){
	var filter;

	if( !options.include ){
		options.include = [
			"**/*.html"
		];
	}

	filter = createFilter( options.include, options.exclude );

	return {
		"name": "lit-html",
		transform( code, id ){
			var result;

			if( filter( id ) ){
				result = {
					"code": template( code ),
					"map": { "mappings": "" }
				};
			}

			return result;
		}
	};
}
